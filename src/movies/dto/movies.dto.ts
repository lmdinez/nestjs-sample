import { ApiProperty } from "@nestjs/swagger";

export class MoviesDto {
    @ApiProperty({ type: Number, description: "id" })
    id: number;

    @ApiProperty({ type: String, description: "Firstname" })
    movieName: string;

    @ApiProperty({ type: String, description: "lastName" })
    MovieImgBase64: string;

    @ApiProperty({ type: String, description: "email" })
    description: string;

}
