import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MoviesController } from './controller/controller.controller';
import { MoviesEntity } from './entity/movies.entity';
import { MoviesService } from './service/movies/movies.service';

@Module({
    imports: [TypeOrmModule.forFeature([MoviesEntity])],
    controllers: [MoviesController],
    providers: [MoviesService]
})
export class MoviesModule { }
