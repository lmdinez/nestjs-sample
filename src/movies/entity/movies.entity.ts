import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('movie')
export class MoviesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
        unique: true
    })
    movieName: string;

    @Column()
    MovieImgBase64: string;

    @Column()
    description: string;

}
