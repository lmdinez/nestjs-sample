import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';
import { MoviesDto } from '../dto/movies.dto';
import { MoviesService } from '../service/movies/movies.service';

@Controller('movie')
export class MoviesController {
    constructor(
        private movieService: MoviesService
    ) {

    }
    @Get()
    async getAllMovies() {
        return {
            statusCode: HttpStatus.OK,
            data: await this.movieService.getAllMovies(),
        };
    }

    @Post()
    @ApiCreatedResponse({ description: 'Movie creation' })
    @ApiBody({ type: MoviesDto })
    async createMovie(@Body() data: MoviesDto) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Movie added successfully',
            data: await this.movieService.createMovie(data),
        };
    }

    @Get(':id')
    @ApiCreatedResponse({ description: 'Get by Movie' })
    async getByMovie(@Param('id') id: number) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.movieService.getByMovie(id),
        };
    }

    @Put(':id')
    async updateMovie(@Param('id') id: number, @Body() data: Partial<MoviesDto>) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Movie update successfully',
            data: await this.movieService.updateMovie(id, data),
        };
    }

    @Delete(':id')
    async deleteMovie(@Param('id') id: number) {
        await this.movieService.deleteByMovie(id);
        return {
            statusCode: HttpStatus.OK,
            message: 'Movie deleted successfully',
        };
    }

}
